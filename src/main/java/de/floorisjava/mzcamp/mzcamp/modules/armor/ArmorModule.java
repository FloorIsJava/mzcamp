/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armor;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.armor.listener.BindingHelmetListener;
import de.floorisjava.mzcamp.mzcamp.modules.armor.listener.NinjaSandalsListener;
import de.floorisjava.mzcamp.mzcamp.modules.armor.listener.PluviasTempestListener;

/**
 * Handles custom armor.
 */
public class ArmorModule extends BaseModule {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ArmorModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(BindingHelmetListener::new);
        addListener(NinjaSandalsListener::new);
        addListener(PluviasTempestListener::new);
    }
}
