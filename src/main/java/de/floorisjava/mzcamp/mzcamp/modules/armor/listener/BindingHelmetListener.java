/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armor.listener;

import de.floorisjava.mzcamp.mzcamp.CustomItem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Objects;
import java.util.Random;

/**
 * Implements binding helmet functionality.
 */
public class BindingHelmetListener implements Listener {

    /**
     * A random number generator.
     */
    private static final Random RANDOM = new Random();

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
            return;
        }
        final LivingEntity attacked = (LivingEntity) event.getEntity();

        final ItemStack helmet = Objects.requireNonNull(attacked.getEquipment()).getHelmet();
        if (!CustomItem.BINDING_HELMET.isItem(helmet)) {
            return;
        }

        if (RANDOM.nextDouble() > 0.2) {
            return;
        }

        final LivingEntity damager = (LivingEntity) event.getDamager();
        damager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40, 0, false, false, false));
    }
}
