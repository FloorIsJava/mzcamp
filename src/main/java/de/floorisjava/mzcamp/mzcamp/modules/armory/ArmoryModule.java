/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armory;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.armory.command.ArmoryCommand;
import de.floorisjava.mzcamp.mzcamp.modules.armory.listener.ArmoryListener;

/**
 * Handles /armory.
 */
public class ArmoryModule extends BaseModule {

    /**
     * The armory manager.
     */
    private final ArmoryManager armoryManager = new ArmoryManager();

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ArmoryModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> new ArmoryListener(armoryManager));
    }

    @Override
    protected void startModule() {
        getModuleManager().getCommand("armory").setExecutor(new ArmoryCommand(armoryManager));
    }

    @Override
    protected void stopModule() {
        getModuleManager().getCommand("armory").setExecutor(null);
    }
}
