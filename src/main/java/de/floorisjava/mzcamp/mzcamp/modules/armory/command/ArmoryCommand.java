/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armory.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.armory.ArmoryManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /armory.
 */
public class ArmoryCommand extends LeafCommand {

    /**
     * The armory manager.
     */
    private final ArmoryManager armoryManager;

    /**
     * Constructor.
     */
    public ArmoryCommand(final ArmoryManager armoryManager) {
        super("/armory - Opens the armory", "mzcamp.armory");
        this.armoryManager = armoryManager;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only for players!");
            return;
        }
        armoryManager.openArmory((Player) sender);
    }
}
