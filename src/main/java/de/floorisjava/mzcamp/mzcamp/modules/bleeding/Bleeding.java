/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.bleeding;

import de.floorisjava.mc.plugin.base.module.ModuleStatus;
import org.bukkit.entity.Player;

/**
 * Manages bleeding.
 */
public interface Bleeding extends ModuleStatus {

    /**
     * Starts bleeding for the given player.
     *
     * @param who The player.
     */
    void startBleeding(final Player who);

    /**
     * Checks whether the given player is bleeding.
     *
     * @param who The player.
     * @return {@code true} if the player is bleeding.
     */
    boolean isBleeding(final Player who);

    /**
     * Stops the bleeding of the player, if bleeding.
     *
     * @param who             The player.
     * @param suppressMessage Whether to suppress the stop message.
     */
    void stopBleeding(final Player who, final boolean suppressMessage);
}
