/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.butcher.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Zombie;

/**
 * /butcher.
 */
public class ButcherCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public ButcherCommand() {
        super("/butcher - Kills zombies and dropped items", "mzcamp.butcher");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        Bukkit.getWorlds().forEach(w -> w.getEntities().stream()
                .filter(x -> x instanceof Zombie || x instanceof Item)
                .forEach(Entity::remove));
        sender.sendMessage("§aCleared zombies and items.");
    }
}
