/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable.listener;

import de.floorisjava.mc.plugin.base.util.ItemUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import lombok.RequiredArgsConstructor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Objects;

/**
 * Implements cocaine functionality.
 */
@RequiredArgsConstructor
public class CocaineListener implements Listener {

    /**
     * The owning plugin, for scheduling.
     */
    private final Plugin plugin;

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (!event.hasItem() || (event.getAction() != Action.RIGHT_CLICK_AIR
                                 && event.getAction() != Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final ItemStack item = Objects.requireNonNull(event.getItem());
        if (!CustomItem.COCAINE.isItem(item)) {
            return;
        }

        ItemUtil.consumeOne(item);
        event.getPlayer().getInventory().setItemInMainHand(item);

        final Player who = event.getPlayer();
        who.getWorld().playSound(who.getEyeLocation(), Sound.ENTITY_PLAYER_BURP, 1.0f, 1.0f);
        who.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 1, false, false, false));
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            who.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 0, false, false, false));
            who.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1, false, false, false));
        }, 600L);
    }
}
