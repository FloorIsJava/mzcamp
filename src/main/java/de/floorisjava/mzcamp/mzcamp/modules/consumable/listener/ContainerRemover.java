/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable.listener;

import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionType;

import java.util.Objects;

/**
 * Removes food containers.
 */
@RequiredArgsConstructor
public class ContainerRemover implements Listener {

    /**
     * The owning plugin, for scheduling.
     */
    private final Plugin plugin;

    @EventHandler
    public void onPlayerItemConsume(final PlayerItemConsumeEvent event) {
        if (event.getItem().getType() == Material.MILK_BUCKET) {
            clearHandItem(event.getPlayer(), event.getItem());
        }
        if (event.getItem().getType() == Material.POTION) {
            final PotionMeta meta = (PotionMeta) Objects.requireNonNull(event.getItem().getItemMeta());
            if (meta.getBasePotionData().getType() == PotionType.INSTANT_HEAL) {
                clearHandItem(event.getPlayer(), event.getItem());
            }
        }
    }

    /**
     * Clears the given item from the player's hands.
     *
     * @param who  The player.
     * @param what The item.
     */
    private void clearHandItem(final Player who, final ItemStack what) {
        if (what.isSimilar(who.getInventory().getItemInMainHand())) {
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,
                    () -> who.getInventory().setItemInMainHand(null));
        } else {
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,
                    () -> who.getInventory().setItemInOffHand(null));
        }
    }
}
