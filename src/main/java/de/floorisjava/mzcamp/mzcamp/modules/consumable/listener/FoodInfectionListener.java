/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable.listener;

import de.floorisjava.mzcamp.mzcamp.modules.infection.Infection;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import java.util.Random;

/**
 * Handles food effects on infections.
 */
@RequiredArgsConstructor
public class FoodInfectionListener implements Listener {

    /**
     * Random number generator.
     */
    private final Random random = new Random();

    /**
     * Infection feature interface.
     */
    private final Infection infection;

    @EventHandler
    public void onPlayerItemConsume(final PlayerItemConsumeEvent event) {
        if (event.getItem().getType() == Material.ROTTEN_FLESH) {
            if (random.nextInt(20) == 0) {
                infection.startInfection(event.getPlayer());
            }
        } else if (event.getItem().getType() == Material.MILK_BUCKET) {
            infection.stopInfection(event.getPlayer(), false);
        }
    }
}
