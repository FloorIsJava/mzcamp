/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.fly.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /fly.
 */
public class FlyCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public FlyCommand() {
        super("/fly [target] - Toggles fly", "mzcamp.fly");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (args.length == 1) {
            final Player target = Bukkit.getPlayer(args[0]);
            if (target != null) {
                target.setAllowFlight(!target.getAllowFlight());
                if (target.getAllowFlight()) {
                    target.sendMessage("§eYou can now fly.");
                    sender.sendMessage(String.format("§e%1$s can now fly.", target.getName()));
                } else {
                    target.sendMessage("§eYou can no longer fly.");
                    sender.sendMessage(String.format("§e%1$s can no longer fly.", target.getName()));
                }
            } else {
                sender.sendMessage(String.format("§cUnknown target player %1$s.", args[0]));
            }
        } else {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§cOnly for players!");
                return;
            }

            final Player who = (Player) sender;
            who.setAllowFlight(!who.getAllowFlight());
            if (who.getAllowFlight()) {
                who.sendMessage("§eYou can now fly.");
            } else {
                who.sendMessage("§eYou can no longer fly.");
            }
        }
    }
}
