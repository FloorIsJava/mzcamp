/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.heal.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.bleeding.Bleeding;
import de.floorisjava.mzcamp.mzcamp.modules.infection.Infection;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.Objects;

/**
 * /heal.
 */
public class HealCommand extends LeafCommand {

    /**
     * The bleeding feature API.
     */
    private final Bleeding bleeding;

    /**
     * The infection feature API.
     */
    private final Infection infection;

    /**
     * Constructor.
     *
     * @param bleeding  The bleeding API.
     * @param infection The infection API.
     */
    public HealCommand(final Bleeding bleeding, final Infection infection) {
        super("/heal [target] - Heals the target.", "mzcamp.heal");
        this.bleeding = bleeding;
        this.infection = infection;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (args.length == 1) {
            final Player target = Bukkit.getPlayer(args[0]);
            if (target != null) {
                healPlayer(target);
                sender.sendMessage(String.format("§e%1$s has been healed.", target.getName()));
            } else {
                sender.sendMessage(String.format("§cUnknown target player %1$s.", args[0]));
            }
        } else {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§cOnly for players!");
                return;
            }

            healPlayer((Player) sender);
        }
    }

    /**
     * Heals the given player.
     *
     * @param target The player.
     */
    private void healPlayer(final Player target) {
        target.setFireTicks(0);

        final double maxHealth = Objects.requireNonNull(target.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getValue();
        target.setHealth(maxHealth);

        target.setFoodLevel(20);
        target.setSaturation(20);
        target.setExhaustion(0);

        target.setLevel(20);

        target.getActivePotionEffects().stream().map(PotionEffect::getType).forEach(target::removePotionEffect);

        bleeding.stopBleeding(target, false);
        infection.stopInfection(target, false);

        target.sendMessage("§aYou have been healed.");
    }
}
