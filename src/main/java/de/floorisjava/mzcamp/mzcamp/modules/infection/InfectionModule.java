/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.infection;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Handles zombie infection.
 */
public class InfectionModule extends BaseModule implements Infection, Listener {

    /**
     * Random number generator.
     */
    private final Random random = new Random();

    /**
     * The currently bleeding players, mapped to their task ID.
     */
    private final Map<UUID, Integer> infectedPlayers = new HashMap<>();

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public InfectionModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @Override
    protected void stopModule() {
        // This will stop tasks from recurring.
        infectedPlayers.clear();
    }

    @Override
    public void startInfection(final Player who) {
        if (!infectedPlayers.containsKey(who.getUniqueId())) {
            // Sentinel value to pass initial check...
            infectedPlayers.put(who.getUniqueId(), -1);
            infectionTick(who);
        }
    }

    @Override
    public void stopInfection(final Player who, final boolean suppressMessage) {
        if (infectedPlayers.containsKey(who.getUniqueId())) {
            getModuleManager().getServer().getScheduler().cancelTask(infectedPlayers.remove(who.getUniqueId()));
            if (!suppressMessage) {
                who.sendMessage("§aI feel much better...");
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Zombie) {
            if (random.nextInt(100) < 3) {
                startInfection((Player) event.getEntity());
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        stopInfection(event.getEntity(), true);
    }

    /**
     * Applies infection effects to the given player.
     *
     * @param who The player.
     */
    private void infectionTick(final Player who) {
        if (!infectedPlayers.containsKey(who.getUniqueId())) {
            return;
        }

        if (who.getHealth() >= 2) {
            who.damage(1);
        }

        who.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 100, 1, false, false, false));
        who.sendMessage("§4Oof... I don't feel so good...");

        final int taskId = getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                () -> infectionTick(who), random.nextInt(200) + 200);
        infectedPlayers.put(who.getUniqueId(), taskId);
    }
}
