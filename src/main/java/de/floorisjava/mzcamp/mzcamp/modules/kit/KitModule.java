/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.kit.command.DeleteKitCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.command.KitCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.command.KitsCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.command.SetKitCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.model.Kit;
import de.floorisjava.mzcamp.mzcamp.modules.kit.model.KitManager;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

/**
 * Handles /kit, /kits, /setkit.
 */
public class KitModule extends BaseModule {

    /**
     * The kit manager.
     */
    private KitManager kitManager;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public KitModule(final ModuleManager moduleManager) {
        super(moduleManager);

        ConfigurationSerialization.registerClass(Kit.class);
        ConfigurationSerialization.registerClass(KitManager.class);
    }

    @Override
    protected void startModule() {
        kitManager = getConfiguration("kits.yml", true).getConfig().getSerializable("kits", KitManager.class);
        if (kitManager == null) {
            kitManager = new KitManager();
        }

        getModuleManager().getCommand("kit").setExecutor(new KitCommand(kitManager));
        getModuleManager().getCommand("kits").setExecutor(new KitsCommand(kitManager));
        getModuleManager().getCommand("delkit").setExecutor(new DeleteKitCommand(kitManager));
        getModuleManager().getCommand("setkit").setExecutor(new SetKitCommand(kitManager));
    }

    @Override
    protected void stopModule() {
        getConfiguration("kits.yml", true).getConfig().set("kits", kitManager);

        getModuleManager().getCommand("kit").setExecutor(null);
        getModuleManager().getCommand("kits").setExecutor(null);
        getModuleManager().getCommand("delkit").setExecutor(null);
        getModuleManager().getCommand("setkit").setExecutor(null);
    }
}
