/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.model.KitManager;
import org.bukkit.command.CommandSender;

import java.util.Collection;

/**
 * /kits
 */
public class KitsCommand extends LeafCommand {

    /**
     * The kit manager.
     */
    private final KitManager kitManager;

    /**
     * Constructor.
     */
    public KitsCommand(final KitManager kitManager) {
        super("/kits - Shows all kits", "mzcamp.kits");
        this.kitManager = kitManager;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Collection<String> kits = kitManager.getKitNames();
        if (kits.isEmpty()) {
            sender.sendMessage("§cNo kits available!");
        } else {
            sender.sendMessage(String.format("§eAvailable kits: %1$s", String.join(", ", kits)));
        }
    }
}
