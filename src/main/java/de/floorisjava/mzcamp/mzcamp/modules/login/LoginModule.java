/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.login;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

/**
 * Handles player logins.
 */
public class LoginModule extends BaseModule implements Listener {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public LoginModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        event.setJoinMessage(String.format("§8[§a+§8] %1$s", event.getPlayer().getDisplayName()));
        event.getPlayer().sendMessage("");
        event.getPlayer().sendMessage("  §a§lPraise the Lilypads");
        event.getPlayer().sendMessage("    §8v" + getModuleManager().getDescription().getVersion());
        event.getPlayer().sendMessage("");
        event.getPlayer().sendMessage("§8Bugs? Issues? Suggestions? → https://floorisjava.de/mzcamp");

        boolean hasNearby = false;
        final List<Entity> nearbyEntities = event.getPlayer().getNearbyEntities(50.0, 50.0, 50.0);
        for (final Entity entity : nearbyEntities) {
            if (entity instanceof Player) {
                hasNearby = true;
                entity.sendMessage("§cSounds like someone is nearby...");
                ((Player) entity).playSound(event.getPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 4.0f, 1.0f);
            }
        }

        if (hasNearby) {
            event.getPlayer().sendMessage("§cSounds like someone is nearby...");
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 4.0f, 1.0f);
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 600, 0, false, false, false));
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        event.setQuitMessage(String.format("§8[§c-§8] %1$s", event.getPlayer().getDisplayName()));
    }
}
