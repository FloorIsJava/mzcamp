/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.logout;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.logout.command.LogoutCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * Handles /logout.
 */
public class LogoutModule extends BaseModule implements Listener, Logout {

    /**
     * The players currently in logout.
     */
    private final Collection<UUID> inLogout = new HashSet<>();

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public LogoutModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @Override
    protected void startModule() {
        getModuleManager().getCommand("logout").setExecutor(new LogoutCommand(this));
    }

    @Override
    protected void stopModule() {
        getModuleManager().getCommand("logout").setExecutor(null);
        inLogout.clear();
    }

    @Override
    public void startLogout(final Player who) {
        if (!inLogout.contains(who.getUniqueId())) {
            inLogout.add(who.getUniqueId());
            runLogoutTask(who, 15);
        }
    }

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event) {
        if (inLogout.contains(event.getPlayer().getUniqueId())) {
            if (event.getFrom().getWorld() != event.getTo().getWorld()) {
                inLogout.remove(event.getPlayer().getUniqueId());
                return;
            }

            final double distance = event.getFrom().distanceSquared(event.getTo());
            if (distance >= 0.01) {
                inLogout.remove(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!inLogout.contains(event.getEntity().getUniqueId())) {
            return;
        }

        final Entity issuer;
        if (event.getDamager() instanceof Player) {
            issuer = event.getDamager();
        } else if (event.getDamager() instanceof Projectile) {
            if (((Projectile) event.getDamager()).getShooter() instanceof Entity) {
                issuer = (Entity) ((Projectile) event.getDamager()).getShooter();
            } else {
                issuer = null;
            }
        } else if (event.getDamager() instanceof Tameable) {
            if (((Tameable) event.getDamager()).getOwner() instanceof Entity) {
                issuer = (Entity) ((Tameable) event.getDamager()).getOwner();
            } else {
                issuer = null;
            }
        } else {
            issuer = null;
        }

        if (issuer instanceof Zombie || issuer instanceof Player) {
            inLogout.remove(event.getEntity().getUniqueId());
        }
    }

    /**
     * Runs the logout timer task for the given player.
     *
     * @param who         The player.
     * @param secondsLeft The number of seconds left until logout.
     */
    private void runLogoutTask(final Player who, final int secondsLeft) {
        if (!inLogout.contains(who.getUniqueId())) {
            who.sendMessage("§cLogout cancelled!");
            return;
        }

        if (secondsLeft == 0) {
            inLogout.remove(who.getUniqueId());
            Bukkit.broadcastMessage(String.format("§e%1$s has logged out.", who.getName()));
            who.kickPlayer("§aLogout successful.");
        } else {
            who.sendMessage(String.format("§eLogout in %1$d seconds.", secondsLeft));
            getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                    () -> runLogoutTask(who, secondsLeft - 1), 20L);
        }
    }
}
