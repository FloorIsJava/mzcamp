/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.protection;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Protects the world against interaction by non-privileged players.
 */
public class ProtectionModule extends BaseModule implements Listener {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ProtectionModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent event) {
        if (event.getBlockPlaced().getType() == Material.COBWEB) {
            return;
        }
        cancelIfCanNotBuild(event, event.getPlayer());
    }

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.COBWEB) {
            return;
        }
        cancelIfCanNotBuild(event, event.getPlayer());
    }

    @EventHandler
    public void onEntityExplode(final EntityExplodeEvent event) {
        event.blockList().clear();
    }

    @EventHandler
    public void onBlockExplode(final BlockExplodeEvent event) {
        event.blockList().clear();
    }

    @EventHandler
    public void onBlockBurn(final BlockBurnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockIgnite(final BlockIgniteEvent event) {
        switch (event.getCause()) {
            case LAVA:
            case SPREAD:
                event.setCancelled(true);
                break;
        }
    }

    /**
     * Cancels an event if the player can not build.
     *
     * @param cancellable The event.
     * @param who         The player.
     */
    private void cancelIfCanNotBuild(final Cancellable cancellable, final Player who) {
        cancellable.setCancelled(who.getGameMode() != GameMode.CREATIVE);
    }
}
