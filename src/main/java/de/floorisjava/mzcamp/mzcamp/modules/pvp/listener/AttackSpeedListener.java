/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pvp.listener;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Objects;

/**
 * Adjusts the attack speed to pre-1.9 values.
 */
public class AttackSpeedListener implements Listener {

    /**
     * Increases the attack speed for the given player.
     *
     * @param who The player.
     */
    public static void increaseAttackSpeed(final Player who) {
        Objects.requireNonNull(who.getAttribute(Attribute.GENERIC_ATTACK_SPEED)).setBaseValue(200.0);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        increaseAttackSpeed(event.getPlayer());
    }
}
