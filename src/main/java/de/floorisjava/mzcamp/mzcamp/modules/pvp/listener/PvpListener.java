/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pvp.listener;

import de.floorisjava.mzcamp.mzcamp.modules.pvp.Pvp;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Handles disabled pvp.
 */
@RequiredArgsConstructor
public class PvpListener implements Listener {

    /**
     * The module interface.
     */
    private final Pvp pvp;

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!pvp.isPvpDisabled()) {
            return;
        }

        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        final Entity damager = event.getDamager();
        if (damager instanceof Player) {
            event.setCancelled(true);
        } else if (damager instanceof Projectile) {
            if (((Projectile) damager).getShooter() instanceof Player) {
                event.setCancelled(true);
            }
        } else if (damager instanceof Tameable) {
            if (((Tameable) damager).getOwner() instanceof Player) {
                event.setCancelled(true);
            }
        }
    }
}
