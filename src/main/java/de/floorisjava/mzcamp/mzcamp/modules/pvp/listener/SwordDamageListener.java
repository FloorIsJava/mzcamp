/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pvp.listener;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

/**
 * Adapts sword damage to custom values.
 */
public class SwordDamageListener implements Listener {

    @EventHandler
    public void onPlayerItemHeld(final PlayerItemHeldEvent event) {
        adaptDamage(event.getPlayer().getInventory().getItem(event.getNewSlot()));
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        for (int idx = 0; idx < 9; ++idx) {
            adaptDamage(event.getPlayer().getInventory().getItem(idx));
        }
    }

    /**
     * Adapts the damage of an item, if possible.
     *
     * @param item The item.
     */
    private void adaptDamage(final ItemStack item) {
        if (item == null) {
            return;
        }

        double damage;
        switch (item.getType()) {
            case WOODEN_SWORD:
            case GOLDEN_SWORD:
                damage = 1.32;
                break;
            case STONE_SWORD:
                damage = 2.48;
                break;
            case IRON_SWORD:
                damage = 3.9;
                break;
            case DIAMOND_SWORD:
                damage = 5.0;
                break;
            default:
                return;
        }

        final ItemMeta meta = Objects.requireNonNull(item.getItemMeta());
        final Collection<AttributeModifier> modifiers = meta.getAttributeModifiers(Attribute.GENERIC_ATTACK_DAMAGE);
        if (modifiers != null && !modifiers.isEmpty()) {
            return;
        }

        final AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "swordDamage", damage,
                AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND);
        meta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, modifier);
        item.setItemMeta(meta);
    }
}
