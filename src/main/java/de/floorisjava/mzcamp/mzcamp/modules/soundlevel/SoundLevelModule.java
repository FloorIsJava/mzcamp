/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.soundlevel;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Implements a sound level feature that judges players sneakiness.
 */
public class SoundLevelModule extends BaseModule {

    /**
     * A random number generator.
     */
    private final Random random = new Random();

    /**
     * Maps UUIDs to their last known location.
     */
    private final Map<UUID, Location> positionCache = new HashMap<>();

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public SoundLevelModule(final ModuleManager moduleManager) {
        super(moduleManager);
    }

    @Override
    protected void startModule() {
        addTask(getModuleManager().getServer().getScheduler().scheduleSyncRepeatingTask(getModuleManager(),
                this::judgePlayers, 20L, 4L));
    }

    /**
     * Judges players' sneakiness.
     */
    private void judgePlayers() {
        for (final Player who : Bukkit.getOnlinePlayers()) {
            if (who.getGameMode() != GameMode.SURVIVAL) {
                continue;
            }

            final Location newPosition = who.getLocation();
            final Location oldPosition = positionCache.getOrDefault(who.getUniqueId(), newPosition);
            positionCache.put(who.getUniqueId(), newPosition);
            oldPosition.setY(newPosition.getY());

            final double horizontalMovement = (oldPosition.distanceSquared(newPosition) / 3.0) * 0.4;
            final double verticalMovement = who.getVelocity().lengthSquared();

            double sneakLevel = 1.0;
            sneakLevel -= horizontalMovement + verticalMovement;
            sneakLevel *= who.isSprinting() ? 0.5 : 1;
            sneakLevel -= random.nextDouble() * 0.15;
            sneakLevel *= who.isSneaking() ? 1.2 : 1;
            sneakLevel = Math.max(Math.min(1.0, sneakLevel), 0.0);
            who.sendExperienceChange((float) (1.0 - sneakLevel * 0.8));
        }
    }
}
