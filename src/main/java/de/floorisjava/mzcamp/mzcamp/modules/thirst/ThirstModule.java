/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.thirst;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionType;

import java.util.Random;

/**
 * Handles the thirst feature.
 */
public class ThirstModule extends BaseModule implements Listener {

    /**
     * Random number generator.
     */
    private final Random random = new Random();

    /**
     * The latest task ID for thirst ticking.
     */
    private int latestTaskId = -1;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ThirstModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @Override
    protected void startModule() {
        latestTaskId = getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                this::thirstTick, 1200L);
    }

    @Override
    protected void stopModule() {
        getModuleManager().getServer().getScheduler().cancelTask(latestTaskId);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (event.getPlayer().getFirstPlayed() == 0
            || System.currentTimeMillis() - event.getPlayer().getFirstPlayed() < 5000) {
            event.getPlayer().setLevel(20);
        }
    }

    @EventHandler
    public void onPlayerRespawn(final PlayerRespawnEvent event) {
        getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                () -> event.getPlayer().setLevel(20));
    }

    @EventHandler
    public void onPlayerItemConsume(final PlayerItemConsumeEvent event) {
        if (event.getItem().getType() == Material.POTION) {
            final PotionMeta meta = (PotionMeta) event.getItem().getItemMeta();
            if (meta != null && meta.getBasePotionData().getType() == PotionType.WATER) {
                event.getPlayer().setLevel(20);
                event.getPlayer().sendMessage("§bAh, much better!");
            }
        }
    }

    /**
     * Performs a thirst tick.
     */
    private void thirstTick() {
        for (final Player who : Bukkit.getOnlinePlayers()) {
            if (who.getGameMode() != GameMode.SURVIVAL) {
                continue;
            }

            if (who.getLevel() >= 1) {
                who.setLevel(who.getLevel() - 1);
            }

            switch (who.getLevel()) {
                case 0:
                    who.damage(1000);
                    break;
                case 1:
                    who.sendMessage("§4I'm going to be in trouble if I don't find water...");
                    break;
                case 5:
                    who.sendMessage("§6I should probably find water soon...");
                    break;
                case 10:
                    who.sendMessage("§eMy throat feels a bit parched...");
                    break;
                default:
                    break;
            }
        }

        latestTaskId = getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                this::thirstTick, 900L + random.nextInt(600));
    }
}
