/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.ui.listener;

import de.floorisjava.mzcamp.mzcamp.modules.ui.util.ChatUtil;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Handles chat formatting.
 */
public class ChatFormatter implements Listener {

    /**
     * The Vault Chat API.
     */
    private final Chat chat;

    /**
     * The cached player chat formats.
     */
    private final Map<UUID, String> playerFormats = new HashMap<>();

    /**
     * Set of players that can use chat formatting.
     */
    private final Set<UUID> canUseChatFormats = new HashSet<>();

    /**
     * Constructor.
     *
     * @param chat The vault chat API.
     */
    public ChatFormatter(final Chat chat) {
        this.chat = chat;
        Bukkit.getOnlinePlayers().forEach(this::createPlayerFormat);
        Bukkit.getOnlinePlayers().stream()
                .filter(p -> p.hasPermission("mzcamp.colorchat"))
                .map(Player::getUniqueId)
                .forEach(canUseChatFormats::add);
    }

    @EventHandler
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        synchronized (playerFormats) {
            event.setFormat(playerFormats.get(event.getPlayer().getUniqueId()));
        }
        synchronized (canUseChatFormats) {
            if (canUseChatFormats.contains(event.getPlayer().getUniqueId())) {
                event.setMessage(ChatUtil.colorize(event.getMessage()));
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        createPlayerFormat(event.getPlayer());
        synchronized (canUseChatFormats) {
            if (event.getPlayer().hasPermission("mzcamp.colorchat")) {
                canUseChatFormats.add(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        synchronized (canUseChatFormats) {
            canUseChatFormats.remove(event.getPlayer().getUniqueId());
        }
    }

    /**
     * Creates the chat format for the given player.
     *
     * @param who The player.
     */
    private void createPlayerFormat(final Player who) {
        final String format = String.format("%1$s%%1$s%2$s » %%2$s", ChatUtil.colorize(chat.getPlayerPrefix(who)),
                ChatUtil.colorize(chat.getPlayerSuffix(who)));
        synchronized (playerFormats) {
            playerFormats.put(who.getUniqueId(), format);
        }
    }
}
