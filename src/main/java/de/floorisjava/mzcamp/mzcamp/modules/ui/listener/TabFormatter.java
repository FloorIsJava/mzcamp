/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.ui.listener;

import de.floorisjava.mzcamp.mzcamp.modules.ui.util.ChatUtil;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Formats the tab player list.
 */
public class TabFormatter implements Listener {

    /**
     * The Vault Chat API.
     */
    private final Chat chat;

    /**
     * Constructor.
     *
     * @param chat The vault chat API.
     */
    public TabFormatter(final Chat chat) {
        this.chat = chat;
        Bukkit.getOnlinePlayers().forEach(this::decoratePlayerList);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        decoratePlayerList(event.getPlayer());
    }

    /**
     * Decorates the player list for the given player.
     *
     * @param who The player.
     */
    public void decoratePlayerList(final Player who) {
        who.setPlayerListHeaderFooter("§a§lPraise the Lilypads\n",
                "\n§7Bugs? Issues? Suggestions?\n→ floorisjava.de/mzcamp");

        who.setPlayerListName(String.format("%1$s%2$s%3$s", ChatUtil.colorize(chat.getPlayerPrefix(who)), who.getName(),
                ChatUtil.colorize(chat.getPlayerSuffix(who))));
    }
}
