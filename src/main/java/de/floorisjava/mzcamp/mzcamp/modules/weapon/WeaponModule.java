/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.CorsairsEdgeListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.FlashGrenadeListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.GrappleListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.GrenadeListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.HurterListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.LoneSwordListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.MuramasaListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.ShotBowListener;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.listener.VampyrListener;

/**
 * Handles custom weapons.
 */
public class WeaponModule extends BaseModule {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public WeaponModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(CorsairsEdgeListener::new);
        addListener(() -> new FlashGrenadeListener(getModuleManager()));
        addListener(GrappleListener::new);
        addListener(GrenadeListener::new);
        addListener(() -> new HurterListener(getModuleManager()));
        addListener(LoneSwordListener::new);
        addListener(MuramasaListener::new);
        addListener(() -> new ShotBowListener(getModuleManager()));
        addListener(VampyrListener::new);
    }
}
