/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mc.plugin.base.util.Cooldown;
import de.floorisjava.mc.plugin.base.util.ItemUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.UUID;

/**
 * Implements the weak grapple.
 */
public class GrappleListener implements Listener {

    /**
     * The grapple cooldown.
     */
    private final Cooldown<UUID> grappleCooldown = new Cooldown<>();

    @EventHandler
    public void onProjectileHit(final ProjectileHitEvent event) {
        if (event.getEntity() instanceof FishHook && event.getEntity().getShooter() instanceof Player) {
            if (event.getHitBlock() != null) {
                final Player shooter = (Player) event.getEntity().getShooter();
                final RayTraceResult rayTrace = shooter.getWorld().rayTraceBlocks(event.getEntity().getLocation(),
                        new Vector(0, -1, 0), 0.25, FluidCollisionMode.NEVER, true);
                if (rayTrace != null && rayTrace.getHitBlock() != null) {
                    shooter.getWorld().playSound(shooter.getEyeLocation(), Sound.BLOCK_IRON_DOOR_CLOSE, 1.0f, 3.0f);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerFish(final PlayerFishEvent event) {
        if (event.getState() != PlayerFishEvent.State.IN_GROUND) {
            return;
        }

        final Player who = event.getPlayer();
        final ItemStack item = who.getInventory().getItemInMainHand();
        if (!CustomItem.WEAK_GRAPPLE.isItem(item)) {
            return;
        }
        final ItemStack itemInHand = item.clone();

        final FishHook hook = event.getHook();
        final Block underHook = hook.getLocation().getBlock().getRelative(BlockFace.DOWN);
        try {
            ItemUtil.damageItem(itemInHand, 2);

            if (hook.getLocation().getY() - 2 <= who.getLocation().getY()) {
                if (grappleCooldown.isOnCooldown(who.getUniqueId())) {
                    who.sendMessage("§cGrapple on cooldown!");
                    hook.getWorld().playSound(hook.getLocation(), Sound.ENTITY_ITEM_BREAK, 5.0f, 1.0f);
                    hook.getWorld().spawnParticle(Particle.BLOCK_DUST, hook.getLocation().add(0, 0.5, 0), 100,
                            0.1, 0.1, 0.1, underHook.getBlockData());
                    return;
                }
                grappleCooldown.addCooldown(who.getUniqueId(), 10000);
            }

            final Vector vec = hook.getLocation().toVector().subtract(who.getLocation().toVector()).multiply(0.2);
            
            who.setVelocity(vec);
            ItemUtil.damageItem(itemInHand, 8);
        } finally {
            who.getInventory().setItemInMainHand(itemInHand);
        }
    }
}
