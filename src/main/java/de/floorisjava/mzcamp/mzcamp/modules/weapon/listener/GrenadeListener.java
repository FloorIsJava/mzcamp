/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mc.plugin.base.util.LocationUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

/**
 * Implements ender pearl grenades.
 */
public class GrenadeListener implements Listener {

    @EventHandler
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        if (event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onProjectileHit(final ProjectileHitEvent event) {
        if (event.getEntityType() == EntityType.ENDER_PEARL) {
            final World world = event.getEntity().getWorld();
            for (final Entity entity : world.getNearbyEntities(event.getEntity().getLocation(), 6, 3, 6)) {
                if (entity instanceof Zombie) {
                    ((Zombie) entity).damage(100.0);
                } else if (entity instanceof LivingEntity) {
                    final double distance = LocationUtil.horizontalDistance(entity.getLocation(),
                            event.getEntity().getLocation());
                    ((LivingEntity) entity).damage(6.0 - distance);
                }
            }

            world.createExplosion(event.getEntity().getLocation(), 0.0f, false, false);
            blastCobwebs(event.getEntity().getLocation());
        }
    }

    /**
     * Blasts cobwebs away near the given location.
     *
     * @param location The location.
     */
    private void blastCobwebs(final Location location) {
        final Block block = location.getBlock();
        final Vector origin = location.toVector();
        for (int dx = -5; dx <= 5; ++dx) {
            for (int dy = -5; dy <= 5; ++dy) {
                for (int dz = -5; dz <= 5; ++dz) {
                    final Block cursor = block
                            .getRelative(BlockFace.SOUTH, dz)
                            .getRelative(BlockFace.EAST, dx)
                            .getRelative(BlockFace.UP, dy);
                    if (cursor.getType() == Material.COBWEB) {
                        cursor.setType(Material.AIR);
                        final FallingBlock falling = cursor.getWorld()
                                .spawnFallingBlock(cursor.getLocation().add(0.5, 0.5, 0.5),
                                        Bukkit.createBlockData(Material.COBWEB));

                        final Vector velocity = cursor.getLocation().toVector().subtract(origin)
                                .normalize().multiply(1.0);
                        velocity.setY(1);
                        falling.setVelocity(velocity);
                        falling.setDropItem(false);
                    }
                }
            }
        }
    }
}
