/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mc.plugin.base.util.Cooldown;
import de.floorisjava.mc.plugin.base.util.ItemUtil;
import de.floorisjava.mc.plugin.base.util.PlayerUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import org.bukkit.Particle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Implements lone sword functionality.
 */
public class LoneSwordListener implements Listener {

    /**
     * Cooldown for lone swords.
     */
    private final Cooldown<UUID> loneSwordCooldown = new Cooldown<>();

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        if (!event.hasItem()) {
            return;
        }
        final ItemStack item = event.getItem();

        if (!CustomItem.LONE_SWORD.isItem(item)) {
            return;
        }

        if (loneSwordCooldown.isOnCooldown(event.getPlayer().getUniqueId())) {
            final long remaining = loneSwordCooldown.getRemainingCooldown(event.getPlayer().getUniqueId());
            event.getPlayer().sendMessage(String.format("§cYou have to wait %1$d seconds before using that again.",
                    remaining / 1000));
            return;
        }
        loneSwordCooldown.addCooldown(event.getPlayer().getUniqueId(), 60000);

        PlayerUtil.heal(event.getPlayer(), 4.0);
        ItemUtil.damageItem(item, 10);
        event.getPlayer().getInventory().setItemInMainHand(item);
        event.getPlayer().getWorld().spawnParticle(Particle.HEART, event.getPlayer().getEyeLocation(), 10,
                0.3, 0.7, 0.3);
    }
}
