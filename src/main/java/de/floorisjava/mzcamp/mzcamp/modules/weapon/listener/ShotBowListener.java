/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mzcamp.mzcamp.CustomItem;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.AbstractArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import java.util.Random;

/**
 * Implements shot bow functionality.
 */
@RequiredArgsConstructor
public class ShotBowListener implements Listener {

    /**
     * A random number generator.
     */
    private final Random random = new Random();

    /**
     * The owning plugin, for scheduling.
     */
    private final Plugin plugin;

    @EventHandler
    public void onEntityShootBow(final EntityShootBowEvent event) {
        final ItemStack bow = event.getBow();
        if (!CustomItem.SHOT_BOW.isItem(bow)) {
            return;
        }

        if (!(event.getProjectile() instanceof Arrow)) {
            return;
        }
        final Arrow eventArrow = (Arrow) event.getProjectile();

        final World world = eventArrow.getWorld();
        final Location spawnLocation = eventArrow.getLocation();
        final Vector direction = eventArrow.getVelocity();
        final Vector normal1 = getNonColinearVector(direction).crossProduct(direction).normalize();
        final Vector normal2 = normal1.clone().crossProduct(direction).normalize();
        for (int i = 0; i < 6; ++i) {
            final double delta1 = random.nextDouble() * 0.3 - 0.15;
            final double delta2 = random.nextDouble() * 0.3 - 0.15;
            final Location arrowLocation = spawnLocation.clone()
                    .add(normal1.clone().multiply(delta1))
                    .add(normal2.clone().multiply(delta2));
            final Arrow arrow = world.spawn(arrowLocation, Arrow.class);

            final double delta3 = random.nextDouble() * 0.3 - 0.15;
            final double delta4 = random.nextDouble() * 0.3 - 0.15;
            final Vector arrowDirection = direction.clone()
                    .add(normal1.clone().multiply(delta3))
                    .add(normal2.clone().multiply(delta4));
            arrow.setVelocity(arrowDirection);

            arrow.setCritical(eventArrow.isCritical());
            arrow.setKnockbackStrength(eventArrow.getKnockbackStrength());
            arrow.setDamage(eventArrow.getDamage());
            arrow.setPierceLevel(eventArrow.getPierceLevel());
            arrow.setShooter(eventArrow.getShooter());
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,
                    () -> arrow.setPickupStatus(AbstractArrow.PickupStatus.CREATIVE_ONLY));
        }
    }

    /**
     * Obtains a non-colinear vector of the given vector. Behavior is undefined for the zero vector.
     *
     * @param vector The vector.
     * @return Another vector.
     */
    private Vector getNonColinearVector(final Vector vector) {
        if (vector.getY() < 0.001 && vector.getZ() < 0.001) {
            return new Vector(0.0, 1.0, 0.0);
        }
        return new Vector(1.0, 0.0, 0.0);
    }
}
