/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.zombies;

import de.floorisjava.mc.plugin.base.util.Cooldown;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Spawns zombies near players.
 */
@RequiredArgsConstructor
public class PlayerZombieSpawner {

    /**
     * A logger.
     */
    private final Logger logger;

    /**
     * Random number generator.
     */
    private final Random random = new Random();

    /**
     * Stores cooldowns until the player can next have a zombie spawn.
     */
    private final Cooldown<UUID> playerCooldowns = new Cooldown<>();

    /**
     * Triggers zombie spawns.
     */
    public void triggerSpawn() {
        logger.finer("triggerSpawn()");
        for (final Player player : Bukkit.getOnlinePlayers()) {
            if (player.getGameMode() != GameMode.SURVIVAL) {
                continue;
            }
            if (random.nextInt(25) == 0) {
                logger.finer("triggerSpawn(): Coin flip passed for " + player.getName());
                if (!playerCooldowns.isOnCooldown(player.getUniqueId()) && belowZombieLimit(player)) {
                    logger.fine("triggerSpawn(): Spawning zombies for " + player.getName());
                    spawnZombieNear(player);
                    playerCooldowns.addCooldown(player.getUniqueId(), 60000);
                }
            }
        }
    }

    /**
     * Spawns a zombie near the player.
     *
     * @param player The player.
     */
    private void spawnZombieNear(final Player player) {
        final double dx = (random.nextDouble() * 15 + 15) * (random.nextBoolean() ? -1 : 1);
        final double dz = (random.nextDouble() * 15 + 15) * (random.nextBoolean() ? -1 : 1);
        final Location target = player.getLocation().add(dx, 0, dz);
        target.setY(player.getWorld().getHighestBlockYAt(target) + 1);
        for (int i = 0; i < random.nextInt(2) + 1; ++i) {
            if (random.nextInt(20) == 0) {
                logger.finer("spawnZombieNear(): Pig Zombie");
                player.getWorld().spawnEntity(target, EntityType.ZOMBIFIED_PIGLIN);
            } else {
                logger.finer("spawnZombieNear(): Zombie");
                player.getWorld().spawnEntity(target, EntityType.ZOMBIE);
            }
        }
    }

    /**
     * Checks whether the player's vicinity is below the zombie limit.
     *
     * @param player The player.
     * @return {@code true} if the player is below the limit.
     */
    private boolean belowZombieLimit(final Player player) {
        return player.getWorld().getNearbyEntities(player.getLocation(), 30, 30, 30,
                e -> e.getType() == EntityType.ZOMBIE || e.getType() == EntityType.ZOMBIFIED_PIGLIN).size() < 10;
    }
}
