/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.zombies;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Zombie;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Objects;

/**
 * Manages zombies.
 */
public class ZombieManager {

    /**
     * Prepares a zombie to be spawned.
     *
     * @param zombie The zombie.
     */
    public void prepareZombie(final Zombie zombie) {
        if (zombie.getEquipment() != null) {
            zombie.getEquipment().clear();
        }

        zombie.setArmsRaised(true);

        Bukkit.getMobGoals().addGoal(zombie, 1, new AiFloat(zombie));
        zombie.getPathfinder().setCanFloat(true);

        zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, zombie.isAdult() ? 3 : 2,
                false, false));

        Objects.requireNonNull(zombie.getAttribute(Attribute.GENERIC_MAX_HEALTH)).setBaseValue(14.0);
        zombie.setHealth(14.0);
    }

    /**
     * Prepares a zombie to be spawned.
     *
     * @param zombie The zombie.
     */
    public void preparePigZombie(final PigZombie zombie) {
        if (zombie.getEquipment() != null) {
            zombie.getEquipment().clear();
        }

        zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, false, false));
        Objects.requireNonNull(zombie.getAttribute(Attribute.GENERIC_MAX_HEALTH)).setBaseValue(0.1);
        zombie.setHealth(0.1);
        zombie.setAngry(true);
        zombie.setAnger(Integer.MAX_VALUE);
    }

    /**
     * Spawns baby zombies at the given location.
     *
     * @param loc The location.
     * @param num The amount.
     */
    public void spawnBabyZombies(final Location loc, final int num) {
        final World world = Objects.requireNonNull(loc.getWorld());
        for (int i = 0; i < num; ++i) {
            Zombie zombie = world.spawn(loc, Zombie.class);
            zombie.setBaby();
            prepareZombie(zombie);
        }
    }
}
