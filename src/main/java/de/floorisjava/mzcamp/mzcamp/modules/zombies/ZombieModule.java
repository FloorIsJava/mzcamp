/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.zombies;

import com.destroystokyo.paper.entity.ai.GoalKey;
import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.zombies.listener.NaturalSpawnFilter;
import de.floorisjava.mzcamp.mzcamp.modules.zombies.listener.PigZombiePinata;
import de.floorisjava.mzcamp.mzcamp.modules.zombies.listener.ZombieLoadListener;
import de.floorisjava.mzcamp.mzcamp.modules.zombies.listener.ZombieResistanceListener;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Mob;

/**
 * Modifies vanilla zombies.
 */
public class ZombieModule extends BaseModule {

    /**
     * Manager for zombie behavior.
     */
    private final ZombieManager zombieManager = new ZombieManager();

    /**
     * Spawns player zombies.
     */
    private final PlayerZombieSpawner playerZombieSpawner;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ZombieModule(final ModuleManager moduleManager) {
        super(moduleManager);
        AiFloat.setGoalKey(GoalKey.of(Mob.class, new NamespacedKey(getModuleManager(), "ai_float")));

        playerZombieSpawner = new PlayerZombieSpawner(moduleManager.getLogger());

        addListener(() -> new NaturalSpawnFilter(zombieManager));
        addListener(() -> new PigZombiePinata(zombieManager));
        addListener(() -> new ZombieLoadListener(zombieManager));
        addListener(ZombieResistanceListener::new);
    }

    @Override
    protected void startModule() {
        addTask(getModuleManager().getServer().getScheduler().scheduleSyncRepeatingTask(getModuleManager(),
                playerZombieSpawner::triggerSpawn, 100L, 100L));
    }
}
