/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.zombies.listener;

import de.floorisjava.mc.plugin.base.util.LocationUtil;
import de.floorisjava.mzcamp.mzcamp.modules.zombies.ZombieManager;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.Objects;

/**
 * Makes pig zombies go brrr.
 */
@RequiredArgsConstructor
public class PigZombiePinata implements Listener {

    /**
     * The zombie manager.
     */
    private final ZombieManager zombieManager;

    @EventHandler
    public void onEntityDeath(final EntityDeathEvent event) {
        if (event.getEntityType() == EntityType.ZOMBIFIED_PIGLIN) {
            final Location loc = event.getEntity().getLocation();
            final World world = Objects.requireNonNull(loc.getWorld());

            event.getDrops().clear();
            world.createExplosion(loc, 0.0f, false, false);
            for (final Entity entity : world.getNearbyEntities(loc, 6.0, 3.0, 6.0)) {
                if (entity instanceof LivingEntity) {
                    final Location locEntity = entity.getLocation();
                    final double distance = LocationUtil.horizontalDistance(locEntity, loc);
                    ((LivingEntity) entity).damage((6.0 - distance) / 6.0 * 5.0);
                }
            }

            zombieManager.spawnBabyZombies(loc, 4);
        }
    }
}
